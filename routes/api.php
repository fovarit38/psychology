<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login');
Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login');
Route::get('_token', 'API\RegisterController@get_token');


Route::get('products', 'API\ProductController@products');
Route::post('products/search', 'API\ProductController@products_search');
Route::get('products/top', 'API\ProductController@productgroups');


Route::get('catalogs', 'API\ProductController@catalogs');
Route::get('catalogs/all', 'API\ProductController@catalogs_all');
Route::get('catalogs/type/{type}', 'API\ProductController@catalogs_type');
Route::get('catalogs/{id}/products', 'API\ProductController@catalog_products');


Route::group(['prefix' => 'basket/'], function () {
    Route::get('current', 'API\OrdersController@basket_current');
    Route::post('add', 'API\OrdersController@basket_add');
    Route::post('amount', 'API\OrdersController@basket_increase');
    Route::delete('delete/{id}', 'API\OrdersController@basket_delete');
    Route::delete('clear', 'API\OrdersController@basket_clear');
});

Route::group(['prefix' => 'favorites/'], function () {
    Route::get('current', 'API\OrdersController@favorites_current');
    Route::post('add', 'API\OrdersController@favorites_add');
    Route::delete('delete/{id}', 'API\OrdersController@favorites_delete');
});

Route::post('push/token', 'API\OrdersController@token_push');



Route::group(['prefix' => 'news/'], function () {
    Route::get('catalog', 'API\ProductController@news_catalog');
    Route::get('one/{id}', 'API\ProductController@news_one');
});

Route::post('address', 'API\ProductController@address');


Route::post('message', 'API\ProductController@message');


Route::group(['prefix' => 'user/'], function () {
    Route::post('edit', 'API\OrdersController@user_edit');
    Route::post('id', 'API\OrdersController@user_id');
    Route::post('check', 'API\RegisterController@user_check');
    Route::post('password/code', 'API\ProductController@user_getcode');
    Route::post('password/reset', 'API\ProductController@user_reset');
});


Route::get('get-time', 'API\ProductController@get_time');


Route::get('/page/{page}/{sheet}', 'API\ProductController@page_gen');

Route::group(['prefix' => 'orders/'], function () {
    Route::post('new', 'API\OrdersController@basket_save');
    Route::get('type', 'API\OrdersController@get_order_type');
    Route::get('history', 'API\OrdersController@basket_history');
    Route::get('history/products', 'API\OrdersController@basket_history_products');
    Route::get('history/repeat/{orders}', 'API\OrdersController@basket_repeat');
});



//Route::middleware('auth:api')->group( function () {
//    Route::resource('products', 'API\ProductController');
//});
