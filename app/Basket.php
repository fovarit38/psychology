<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\Product as ProductResource;

class Basket extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product','Product_id','id');
    }
}
