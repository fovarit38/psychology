<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function getImagesAttribute($value)
    {
        return 'https://'.$_SERVER['SERVER_NAME'].$value;
    }
    public function getContentAttribute($value)
    {
        return strip_tags($value);
    }
}
