<?php


namespace App\Cicada;

use Request;
use Str;


class Payment
{

    protected $params;
    protected $request;

    function __construct($nomer,$summa)
    {

        $this->params = (Request::all());


        $this->request = [
            'pg_merchant_id' => 533689,
            'pg_amount' => $summa,
            'pg_salt' => 'some_random_string',
            'pg_order_id' => $nomer,
            'pg_description' => 'Покупка товаров',
            'pg_result_url' => 'https://'.$_SERVER['SERVER_NAME'].'/paybox/result',
            'pg_success_url' => 'https://'.$_SERVER['SERVER_NAME'].'/paybox/success',
            'pg_failure_url' => 'https://'.$_SERVER['SERVER_NAME'].'/paybox/error',
        ];

    }

    function payment()
    {


        $payBox = $this->request;

        $payBox['pg_testing_mode'] = 1; //add this parameter to request for testing payments

        //if you pass any of your parameters, which you want to get back after the payment, then add them. For example:
        // $request['client_name'] = 'My Name';
        // $request['client_address'] = 'Earth Planet';

        //generate a signature and add it to the array
        ksort($payBox); //sort alphabetically
        array_unshift($payBox, 'payment.php');
        array_push($payBox, '5gvjRPUxLX1P53PD'); //add your secret key (you can take it in your personal cabinet on paybox system)
        $temp_hash = md5(implode(';', $payBox));

        $payBox['pg_sig'] = $temp_hash;

        unset($payBox[0], $payBox[1]);
        $query = http_build_query($payBox);


        return ('https://api.paybox.money/payment.php?' . $query."");

    }


}

