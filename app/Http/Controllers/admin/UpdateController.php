<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use Schema;
use Illuminate\Support\Str;

class UpdateController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update_model(Request $request)
    {
        $files = $request;
        $request = $request->all();

        if ($request["model_name"] == "Notification") {
            $url = "https://fcm.googleapis.com/fcm/send";


            if (mb_strlen(strip_tags($request["name_save"])) >= 45) {
                return redirect()->back()->with("alert", "Ошибка слишком много символов <b>в Названии</b>");
            }
            if (mb_strlen(strip_tags($request["content_save"])) >= 125) {
                return redirect()->back()->with("alert", "Ошибка слишком много символов <b>в описании</b>");
            }


            $android = [
                "to" => "/topics/Test1",
                "data" => [
                    "body" =>
                        strip_tags($request["content_save"]),
                    "title" =>
                        strip_tags($request["name_save"]),
                    "icon" => "ic_launcher",
                    "badge" => 1,
                    "subtitle" => "test",
                    "high_priority" => "high",
                    "show_in_foreground" => true,
                    "sound" => "default",
                    "click_action" => "FLUTTER_NOTIFICATION_CLICK"
                ],
                "priority" => "high",
                "foreground" => true,
                "userInteraction" => true,
                "content_available" => true,
                "mutable_content" => true,
                "show_in_foreground" => true
            ];

            $ios = [
                "to" => "/topics/ios",
                "notification" => [
                    "body" =>
                        strip_tags($request["content_save"]),
                    "title" =>
                        strip_tags($request["name_save"]),
                    "icon" => "ic_launcher",
                    "badge" => 1,
                    "subtitle" => "test",
                    "high_priority" => "high",
                    "show_in_foreground" => true,
                    "sound" => "default",
                    "click_action" => "FLUTTER_NOTIFICATION_CLICK"
                ],
                "priority" => "high",
                "foreground" => true,
                "userInteraction" => true,
                "content_available" => true,
                "mutable_content" => true,
                "show_in_foreground" => true
            ];

            $fields_string = json_encode($android);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: key=AAAATHYSLdk:APA91bGvZFwgHf0CVHTmQ8syZk7_ALHVAEy6I956cg_lfgCzYGvvMNET22IwFPz3X2Pcw0ay4TaPKCTLSc73Tc2y2GXaxk0aNdUptD-5wEWkNL8NmhvoU5Ia9RkfFRVwFfeNfV8JTwDt'
            ));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $mex = json_decode($result);
            if (isset($mex->error)) {
                return redirect()->back()->with("alert", "Ошибка слишком много символов");
            }


            $fields_string = json_encode($ios);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: key=AAAATHYSLdk:APA91bGvZFwgHf0CVHTmQ8syZk7_ALHVAEy6I956cg_lfgCzYGvvMNET22IwFPz3X2Pcw0ay4TaPKCTLSc73Tc2y2GXaxk0aNdUptD-5wEWkNL8NmhvoU5Ia9RkfFRVwFfeNfV8JTwDt'
            ));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $mex = json_decode($result);
            if (isset($mex->error)) {
                return redirect()->back()->with("alert", "Ошибка слишком много символов");
            }

            $result = json_decode($result, true);
        }

        foreach ($_FILES as $nameInput => $fileout) {
            if (isset($request[$nameInput])) {
                if ($files->hasFile($nameInput)) {

                    $photo_path = $files->file($nameInput);
                    $type = explode(".", $photo_path->getClientOriginalName());
                    if (count($type) > 1) {
                        $m_path = Str::random(5) . "_file." . $type[1];
                        $moveTo = 'media/Update/' . str_replace("/", "_", $fileout["type"]) . '/';
                        $photo_path->move($moveTo, $m_path);
                        $request[$nameInput] = "/public/" . $moveTo . $m_path;
                        if (isset($request[$nameInput . "_alt"])) {
                            unset($request[$nameInput . "_alt"]);
                        }
                    }
                } else {
                    unset($request[$nameInput]);
                }
            }
        }


        $id = $request["id"];
        $model_name = $request["model_name"];
        $model = null;
        if ($id == 0) {
            $model = app("\App\\$model_name");
        } else {
            $model = app("\App\\$model_name");
            $model = $model->find($id);
        }

        if ($model_name == "Shop") {
            if (!isset($request["product_id_save"])) {
                $request["product_id_save"] = [];
            }
        }


        if (is_null($model)) {
            return redirect()->back()->with("alert", "Модель не найденна");
        }


        foreach ($request as $key => $input) {
            $input_save = explode("_sa", str_replace("_alt", "", $key));
            $is_save = end($input_save);
            if ("sa" . $is_save == "save") {
                if ($input_save[0] == "product_id" && is_array($input)) {
                    $model->{$input_save[0]} = "," . implode(",", $input) . ",";
                } else {
                    $model->{$input_save[0]} = $input;
                }
            }
        }

        $model->save();


        $columns = Schema::getColumnListing($model->getTable());
        if (in_array("path", $columns) && in_array("id", $columns) && in_array("slug", $columns)) {
            $columSlug = "";
            if (in_array("title", $columns)) {
                $columSlug = "title";
            } else if (in_array("name", $columns)) {
                $columSlug = "name";
            }
            if ($columSlug != "") {

                $dop_path = '';
                if (in_array("catalog_id", $columns) && isset($request["catalog_id_save"])) {
                    $catalog = \App\Catalog::find($request["catalog_id_save"]);
                    if (!is_null($catalog)) {
                        if (isset($catalog->path)) {
                            $dop_path = $catalog->path;
                        }
                    }
                }

                $urlSlug = Urlcode($model->{$columSlug});
                $firstEditSlug = true;
                while (!is_null(\app("\App\\$model_name")->where("slug", $urlSlug)->first())) {
                    $urlSlug .= ($firstEditSlug ? "-" : "") . $model->id;
                    $firstEditSlug = false;
                }
                if ($urlSlug != "") {
                    $model->slug = $urlSlug;
                    $model->path = ($dop_path != '' ? $dop_path . "/" : "") . $model->slug;
                    $model->save();
                }
            }
        }

        if ($model_name == "Order_item") {
            $product_one = \App\Product::find($request["product_id_save"]);
            $model->price = $product_one->price * $request["amount_save"];
            $model->price_old = $product_one->price_old * $request["amount_save"];
            $model->discount = 0;
            $model->status = "new";
            $model->amount = $request["amount_save"];
            $model->orders_id = $request["orders_id_save"];
            $model->title = $product_one->title;
            $model->product_id = $product_one->id;

            if ($id == 0) {
                $order = \App\Order::find($request["orders_id_save"]);
                $order->price += $product_one->price * $request["amount_save"];
                $order->save();
            }

            $model->save();

        }

        return redirect()->to(str_replace("{id}", $model->id, ($request["path"])));
    }


    public function status_set($id, Request $request)
    {

    }

}
