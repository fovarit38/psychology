<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use TableClass;
use \App\Exports\OrdersExport;
use \App\Exports\OrdersOneExport;
use Excel;

class MainController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export_ex()
    {
        return Excel::download(new OrdersExport, 'orders.xlsx');
    }

    public function export_one_ex($id)
    {
        return Excel::download(new OrdersOneExport($id), 'orders.xlsx');
    }

    public function index()
    {

        return view('views.main');
    }


    public function table()
    {
        return view('main');
    }


    public function developer($status)
    {
        if ($status != "true") {
            $status = false;
        }
        Cache::forever('dev', $status);


        return redirect()->back();
    }

    public function s_text()
    {
        $model_name = "StaticText";
        $thead_nav = \App\StaticText::get()->groupby("page");
        $tbody = [];

        $page = '';
        if (isset($_REQUEST["page"])) {
            $tbody = \App\StaticText::where("page", $_REQUEST["page"])->get();
            $page = $_REQUEST["page"];
        }

        $thead = ["id" => "id", "name_key" => "name_key"];

        $table_link = ["/admin/model//", "id"];

        return view('views.s_text', compact("model_name", 'page', "thead_nav", "tbody", "thead", "table_link"));

    }

    public function update_text(Request $request)
    {
        $request = $request->all();

        foreach ($request["save"] as $key => $colum) {
            $s_text = \App\StaticText::where("name_key", $key)->first();
            if (is_array($colum)) {
                $s_text->content = json_encode($colum, JSON_UNESCAPED_UNICODE);
            } else {
                $s_text->content = $colum;
            }
            $s_text->save();
        }
        return redirect()->back();
    }

    public function status_set(Request $request)
    {
        $request = $request->all();
        $orders = \App\Order::find($request["id"]);
        $orders->status = $request["status"];
        $orders->save();
        return redirect()->to(url_custom('/admin/model/Order'));
    }

    public function orders()
    {
        return view('views.orders');
    }
}
