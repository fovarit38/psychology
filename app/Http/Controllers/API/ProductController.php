<?php

namespace App\Http\Controllers\API;

use App\Catalog;
use App\Http\Resources\Order_item as OrdersItemResource;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Product;
use Validator;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\Catalog as CatalogResource;
use App\Http\Resources\ProductGroups as ProductGroupsResource;
use App\Mail\resetShipped;
use App\Mail\mailShipped;
use Mail;
use Illuminate\Support\Str;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $basket;
    protected $users;
    protected $basket_id;

    public function __construct(Request $request)
    {
        $this->users = auth()->guard('api')->user();

        if (isset($this->users->id) || !empty($this->users)) {
            $this->middleware(function ($request, $next) {
                $orders = \App\Order::where("user_id", $this->users->id)->get();
                foreach ($orders as $ordsa) {
                    foreach ($ordsa->items as $producsa) {
                        if (in_array($producsa->product_id, $this->basket_id)) {
                            array_push($this->basket_id, $producsa->product_id);
                        }
                    }
                }
            });
        }
    }

    public function products()
    {
        $nocatalog = array_keys(\App\Catalog::where("visable", "0")->orderby("sort")->get()->groupby("id")->toarray());

        $products = Product::whereNotIn("catalog_id", $nocatalog)->where("visable", "1")->orderby("sort")->get();

        return $this->sendResponse(ProductResource::collection($products), '');
    }

    public function productgroups()
    {
        $productgroups = \App\ProductGroups::get();

        return $this->sendResponse(ProductGroupsResource::collection($productgroups), '');
    }
//
//    public function catalog_top()
//    {
//
//        $return_pg = [];
//        $productGroups = \App\ProductGroups::get();
//        foreach ($productGroups as $pg) {
//            array_push($return_pg,["name"=>$pg->name,"children"=>$pg->items]);
//        }
//        return $this->sendResponse($return_pg, '');
//    }

    public function catalogs()
    {
        $catalog = Catalog::where("visable", "1")->orderby("sort")->get();

        return $this->sendResponse(CatalogResource::collection($catalog), '');
    }

    public function catalog_products($id)
    {
        $catalog = \App\Catalog::where("visable", "1")->where("id", $id)->first();
        if (is_null($catalog)) {
            return $this->sendError('Каталог не найден.');
        }
        $chile = \App\Catalog::where("visable", "1")->where("parent_id", $catalog->id)->first();
        $id_list = [$catalog->id];
        while (!is_null($chile)) {
            array_push($id_list, $chile->id);
            $chile = \App\Catalog::where("visable", "1")->where("parent_id", $chile->id)->first();
        }

        return $this->sendResponse(ProductResource::collection(\App\Product::wherein("catalog_id", $id_list)->orderby("sort")->get()), '');
    }

    public function products_search(Request $request)
    {
        $request = $request->all();
        if (!isset($request["search"])) {
//            return $this->sendError('Нет входных search данных.');
        }

        $products_id = null;
        if (isset($request["tags"])) {
            if (!is_array($request["tags"])) {
                $request["tags"] = [$request["tags"]];
            }
            $products_id = array_keys(\App\Product_tag::where(function ($query) use ($request) {
                foreach ($request["tags"] as $tag) {
                    $query->orWhere('name','LIKE', '%' . $tag . '%');
                }
            })->get()->groupby("products_id")->toarray());
        }

        $nocatalog = array_keys(\App\Catalog::where("visable", "0")->orderby("sort")->get()->groupby("id")->toarray());

        $products = Product::where("title", "LIKE", "%" . (isset($request["search"])?$request["search"]:"") . "%")->where("visable", "1")->whereNotIn("catalog_id", $nocatalog);
        if (!is_null($products_id)) {
            $products = $products->wherein("id", $products_id);
        }
        $products = $products->orderby("sort")->get();
        return $this->sendResponse(ProductResource::collection($products), '');
    }

    public function catalogs_type($type)
    {

        $catalog = \App\Catalog::where("visable", "1")->where("type", $type)->orderby("sort")->get();

        $catalog_return = [];
        foreach ($catalog as $cat_new) {
            $cat_new->products;
            array_push($catalog_return, ["categories" => $cat_new, "categoryName" => $cat_new->title]);
        }
        return $this->sendResponse($catalog_return, '');

    }

    public function catalogs_all()
    {

        $catalog = \App\Catalog::where("visable", "1")->where("type", "0")->orderby("sort")->get();

        $catalog_return = [];
        foreach ($catalog as $cat_new) {
            $cat_new->products;
            $cat_new->children;
            array_push($catalog_return, ["categories" => $cat_new, "categoryName" => $cat_new->title]);
        }
        return $this->sendResponse($catalog_return, '');

    }


    public function get_time()
    {

        $time = \App\Day::where("name_key", date("w", time()))->first();

        $time_control = false;
        if (strtotime($time->fromTime) <= strtotime(date("H:i", time())) && strtotime($time->toTime) >= strtotime(date("H:i", time()))) {
            $time_control = true;
        }

        return $this->sendResponse(["time" => ["from" => $time->fromTime, "to" => $time->toTime], "open" => $time_control], "message");

    }


    public function page_gen($page, $sheet)
    {
        if ($sheet == "aboutus") {
            $head = s_("Заголовок о нас", $page, "", "textarea");
            $text = s_("Страница " . $sheet, $page, "", "textarea");

            $adressa = [
            ];

//            [
//                "name" => "Адреса точек B&C Burger в г. Уральск",
//                "items" => [
//                    ["name" => "Доставка 11.00-22.00", "type" => "point"],
//                    ["name" => "тел: +7 707 226 73 77", "type" => "phone"],
//                    ["name" => "#2 мкр. Строитель 26/3", "type" => "point"],
//                    ["name" => "тел: +7 707 226 73 77", "type" => "phone"],
//                    ["name" => "bc_burger@gmail.com", "type" => "emailemail"],
//                    ["name" => "B&C Burger Street 11.00-22.00", "type" => "title"],
//                    ["name" => "bc_burger@gmail.com", "type" => "email"],
//                    ["name" => "bc_burger_uralsk", "type" => "inst"],
//                ],
//            ]

            foreach (\App\Construction::get() as $consting) {
                $dating = ["name" => $consting->name, "items" => []];
                foreach (\App\Construction_item::where("construction_id", $consting->id)->get() as $itemcon) {
                    array_push($dating["items"], ["name" => $itemcon->name, "type" => $itemcon->type]);
                }
                array_push($adressa, $dating);
            }

            return $this->sendResponse(["Heading" => $head, "SubHeading" => $text, "addresses" => $adressa], '');
        } else {
            $fileText = s_("Страница " . $sheet, $page, "", "textarea");

            return response($fileText, 200)->header('Content-Type', 'text/html');
        }
    }

    public function user_getcode(Request $request)
    {
        $request = $request->all();
        $code = rand(1000, 9000);

        $email = \App\User::where("email", $request["email"])->first();
        if (is_null($email)) {
            return $this->sendError('Пользователь не найден.');
        }
        $email->reset_code = $code;
        $email->save();
        Mail::to($request["email"])->send(new resetShipped($code));

        return $this->sendResponse([], 'код для сброса пароля отправлен');

    }


    public function news_catalog()
    {
        $retsa = [];
        foreach (\App\News::orderby("created_at", "desc")->get() as $news) {
            array_push($retsa, $news->toarray());
        }
        return $this->sendResponse($retsa, '');
    }

    public function news_one($id)
    {

        $news = \App\News::find($id);
        if (is_null($news)) {
            return $this->sendError('Новость  не найдена.');
        }
        return $this->sendResponse($news, '');
    }

    public function user_reset(Request $request)
    {
        $request = $request->all();
        if ($request["password"] != $request["password_confirmation"]) {
            return $this->sendError('Пароль не совпадает.');
        }

        $email = \App\User::where("reset_code", $request["code"])->first();
        if (is_null($email)) {
            return $this->sendError('Код не действителен.');
        }
        $email->reset_code = "";
        $email->password = bcrypt($request['password']);
        $email->save();
        return $this->sendResponse([], '');

    }


    public function message(Request $request)
    {

        Mail::to("bc.burger.uralsk@gmail.com")->send(new mailShipped($request->all()));
        return $this->sendResponse([], '');
    }


    public function address(Request $request)
    {
        $adressa = \App\AdressList::where("name", "LIKE", "%" . $request["search"] . "%")->orderby("sort")->get();

        return $this->sendResponse($adressa->toarray(), '');
    }


}
