<?php

namespace App\Http\Controllers\API;

use App\Catalog;
use App\Mail\mailShipped;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\Catalog as CatalogResource;
use App\Http\Resources\Basket as BasketResource;
use App\Http\Resources\Order as OrderResource;
use Cookie;
use DB;
use App\Http\Resources\Order_item as OrdersItemResource;
use Payment;
use Mail;

class OrdersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Товары в корзине
    protected $basket;
    protected $users;


    public function __construct(Request $request)
    {
        $this->users = auth()->guard('api')->user();

        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
            $this->middleware(function ($request, $next) {
                return $this->sendError('вы не авторизованы');
            });
        }
    }


    public function basket_increase(Request $request)
    {
        if (!isset($request->amount)) {
            return $this->sendError('Нужно отправить amount.');
        } else {
            if ($request->amount <= 0) {
                return $this->sendError('amount не может быть меньше 0.');
            }
        }
        if (!isset($request->basket_id)) {
            return $this->sendError('Нужно отправить basket_id.');
        }
        $basket = \App\Basket::where("user_id", $this->users->id)->where("id", $request->basket_id)->first();
        if (is_null($basket)) {
            return $this->sendError('Запись ' . $request->basket_id . ' в корзине не найдена.');
        }

        $basket->amount = $request->amount;
        $basket->save();
        return $this->sendResponse(BasketResource::collection([$basket]), 'Количество изменено');
    }

    public function basket_history()
    {
        $orders = \App\Order::where("user_id", $this->users->id)->get();
        if (count($orders) == 0) {
            return $this->sendError('Истории заказов нет');
        }

        return $this->sendResponse(OrderResource::collection($orders), 'История заказов');
    }


    public function basket_history_products()
    {
        $orders = \App\Order::where("user_id", $this->users->id)->get();
        if (count($orders) == 0) {
            return $this->sendError('Истории заказов нет');
        }
        $return_producs = [];
        foreach ($orders as $ordsa) {
            foreach (OrdersItemResource::collection($ordsa->items) as $producsa) {
                array_push($return_producs, $producsa);
            }
        }

        return $this->sendResponse($return_producs, 'История заказов');
    }

    public function basket_repeat($orders)
    {
        $orders = \App\Order::where("order_number", $orders)->where("User_id", $this->users->id)->first();
        $basket_add = [];
        if (!is_null($orders)) {

            foreach ($orders->items as $its) {
                //Добавляем в корзину если нет
                $product = \App\Product::find($its->product_id);
                if (is_null($product)) {
                    return $this->sendError('Один из продуктов не найден, перезаказать невозможно.');
                }
            }

            \App\Basket::where("user_id", $this->users->id)->delete();
            foreach ($orders->items as $its) {
                //Добавляем в корзину если нет
                $product = \App\Product::find($its->product_id);
                if (is_null($product)) {
                    return $this->sendError('Один из продуктов не найден, перезаказать невозможно.');
                }

                $basket = \App\Basket::where("user_id", $this->users->id)->where("Product_id", $product->id)->first();
                if (is_null($basket)) {
                    $basket = new \App\Basket;
                    $basket->Product_id = $product->id;
                    $basket->user_id = $orders->User_id;
                    $basket->amount = $its->amount;
                    $basket->save();
                    array_push($basket_add, ["product" => $product, "amount" => $its->amount]);
                }
            }

        }
        if (count($basket_add) > 0) {
            return $this->sendResponse([$basket_add], 'Корзина была изменена');
        }

        return $this->sendError('заказ не найден');
    }

    public function basket_current()
    {

        $basket = \App\Basket::where("user_id", $this->users->id)->get();
        if (count($basket) == 0) {
            return $this->sendError('Корзина пуста.');
        }
        $allPrice = 0;
        foreach ($basket as $bas) {
            $product_one = \App\Product::find($bas->Product_id);
            $allPrice += $product_one->price * $bas->amount;
        }
        return $this->sendResponse(["basket" => BasketResource::collection($basket), "allPrice" => $allPrice], 'Ваша корзина:D');
    }

    public function basket_delete($id)
    {


        //Добавляем в корзину если нет

        $basket = \App\Basket::where("id", $id)->where("user_id", $this->users->id)->first();

        if (is_null($basket)) {
            return $this->sendError('в корзине такой записи нет.');
        }
        $basket->delete();

        return $this->sendResponse([], 'Товар был удален из корзины');
    }

    public function basket_clear()
    {
        $basket = \App\Basket::where("user_id", $this->users->id)->delete();
        return $this->sendResponse([], 'Корзина очищена');
    }

    public function basket_add(Request $request)
    {
        $request = $request->all();


        //Добавляем в корзину если нет
        $product = \App\Product::find($request["id"]);
        if (is_null($product)) {
            return $this->sendError('Продукт не найден.');
        }
        if ($request["amount"] <= 0) {
            return $this->sendError('Нужно выбрать количество продукции.');
        }
        $basket = \App\Basket::where("user_id", $this->users->id)->where("Product_id", $product->id)->first();
        if (!is_null($basket)) {
            return $this->sendError('Товар уже был добавлен в корзину.');
        }
        $basket = new \App\Basket;
        $basket->Product_id = $product->id;
        $basket->user_id = $this->users->id;
        $basket->amount = $request["amount"];
        $basket->save();


        $basket[$request["id"]] = $request["amount"];

        $product->basket_id = $basket->id;

        $resourse = ProductResource::collection([$product]);

        return $this->sendResponse($resourse[0], 'Товар добавлен в корзину');
    }

    public function get_order_type()
    {
        $payment_type_list = ["cash", "online", "kaspi"];
        $payment_type_list_return = [];
        foreach ($payment_type_list as $t_c) {
            $info_ = s_("Оплата через " . $t_c, "Виды оплаты", "", "radio");
            if ($info_ == "1") {
                array_push($payment_type_list_return, $t_c);
            }
        }
        return $this->sendResponse($payment_type_list_return, '');

    }


    public function basket_save(Request $request)
    {
        $request = $request->all();

        if (isset($request["products_id"])) {
            $products_list = \App\Product::wherein("id",explode(",", $request["products_id"]))->get();
            if (count($products_list) > 0) {
                foreach ($products_list as $product_in) {
                    $basket = \App\Basket::where("user_id", $this->users->id)->where("Product_id", $product_in->id)->first();
                    $basket = new \App\Basket;
                    $basket->Product_id = $product_in->id;
                    $basket->user_id = $this->users->id;
                    $basket->amount = "1";
                    $basket->save();
                }
            }else{
                return $this->sendError('Товаров нет.');
            }
        }

        $basket = \App\Basket::where("user_id", $this->users->id)->get();

        if (count($basket) == 0) {
            return $this->sendError('Корзина пуста.');
        }

        $order_code = random_int(100000, 999999);

        $all_summe = 0;
        $order = new \App\Order;
        $order->order_number = $order_code;
        $order->price = 0;
        $order->price_old = 0;
        $order->status = "new";
        $order->User_id = $this->users->id;
        $order->reques_info = json_encode($request);

        $order_type = "cash";

        $payment_type_list = ["cash", "online", "kaspi"];


        if (isset($request["comment"])) {
            $order->comment = $request["comment"];
        }

        if (isset($request["payment_type"])) {
            if (in_array($request["payment_type"], $payment_type_list)) {
                $order_type = $request["payment_type"];
            }
        }

        if (isset($request["address"])) {
            $order->address = $request["address"];
        }
        if (isset($request["name"])) {
            $order->name = $request["name"];
        }
        if (isset($request["phone"])) {
            $order->phone = $request["phone"];
        }
        if (isset($request["email"])) {
            $order->email = $request["email"];
        }

        if (isset($request["payment"])) {
            $order->payment = $request["payment"];
        }

        if (isset($request["delivery"])) {
            $order->delivery = $request["delivery"];
        }
        if (isset($request["delivery_time"])) {
            $order->delivery_time = $request["delivery_time"];
        }
        if (isset($request["delivery_price"])) {
            $order->delivery_price = $request["delivery_price"];
        }
        if (isset($request["surrender"])) {
            $order->surrender = $request["surrender"];
        }

        if (isset($request["apartment"])) {
            $order->apartment = $request["apartment"];
        }
        if (isset($request["entrance"])) {
            $order->entrance = $request["entrance"];
        }
        if (isset($request["district"])) {
            $order->district = $request["district"];
        }


        $order->save();


        foreach ($basket as $basket_one) {
            $product_one = \App\Product::find($basket_one->Product_id);
            $order_new = new \App\Order_item;
            $order_new->price = $product_one->price;
            $order_new->price_old = $product_one->price_old;
            $order_new->discount = 0;
            $order_new->status = "new";
            $order_new->orders_id = $order->id;
            $order_new->amount = $basket_one->amount;
            $order_new->title = $product_one->title;
            $order_new->product_id = $product_one->id;
            $order_new->save();
            $order->price += $product_one->price * $basket_one->amount;
            $order->save();
            $basket_one->forceDelete();
        }


        Mail::to("bc.burger.uralsk@gmail.com")->send(new mailShipped($request));


        if ($order_type == "online") {
            $payment = new Payment($order->order_number, $order->price);
            return $this->sendResponse(["basket" => $order, "allPrice" => $order->price, "payment_type" => $order_type, "redirect_to" => $payment->payment()], $order->order_number);
        } else if ($order_type == "kaspi") {
            return $this->sendResponse(["basket" => $order, "allPrice" => $order->price, "payment_type" => $order_type, "phone" => "+77089681134"], $order->order_number);
        }
        return $this->sendResponse(["basket" => $order, "allPrice" => $order->price, "payment_type" => $order_type], $order->order_number);
    }


    public function favorites_current()
    {

        $basket = \App\Favorite::where("user_id", $this->users->id)->get();
        if (count($basket) == 0) {
            return $this->sendError('в избранных нет товаров.');
        }

        $retsa = [];
        foreach ($basket as $favbaset) {
            $produas = \App\Product::find($favbaset->product_id)->toarray();
            if (!is_null($produas)) {
                $produas["favorit_id"] = $favbaset->id;
                array_push($retsa, $produas);
            }
        }
        return $this->sendResponse($retsa, 'Ваша корзина:D');
    }

    public function favorites_add(Request $request)
    {
        $request = $request->all();


        //Добавляем в корзину если нет
        $product = \App\Product::find($request["id"]);

        if (is_null($product)) {
            return $this->sendError('Продукт не найден.');
        }


        $basket = \App\Favorite::where("user_id", $this->users->id)->where("product_id", $product->id)->first();
        if (!is_null($basket)) {
            return $this->sendError('Товар уже был добавлен в избранное.');
        }

        $basket = new \App\Favorite;
        $basket->product_id = $product->id;
        $basket->user_id = $this->users->id;
        $basket->save();

        $product = $product->toarray();
        $product["favorit_id"] = $basket->id;


        return $this->sendResponse($product, 'Товар добавлен в избранное');
    }

    public function favorites_delete($id)
    {


        //Добавляем в корзину если нет

        $basket = \App\Favorite::where("id", $id)->where("user_id", $this->users->id)->first();

        if (is_null($basket)) {
            return $this->sendError('в избранных такой записи нет.');
        }
        $basket->delete();

        return $this->sendResponse([], 'Товар был удален из избранных');
    }

    public function user_id(Request $request)
    {
        $request = $request->all();
        $user = \App\User::find($this->users->id);
        if (is_null($user)) {
            return $this->sendError('пользователь не найден');
        }
        $user->id_push = $request["id"];
        $user->save();
        return $this->sendResponse($user, 'id сохранен');
    }

    public function user_edit(Request $request)
    {
        $input = $request->all();

        $user = \App\User::find($this->users->id);


        $return_push = [];
        if (isset($input['password']) && isset($input['password_confirmation'])) {
            if ((str_replace(" ", "", $input['password']) != "") && (str_replace(" ", "", $input['password_confirmation']) != "")) {
                if ($input['password'] == $input['password_confirmation']) {
                    if (mb_strlen($input['password']) > 6) {
                        $user->password = bcrypt($input['password']);
                        array_push($return_push, 'password');
                    }
                }
            }
        }

        if (isset($input['email'])) {
            if (str_replace(" ", "", $input['email']) != "") {
                $user->email = ($input['email']);
                array_push($return_push, 'email');
            }
        }

        if (isset($input['tel'])) {
            if (validate_phone_number($input['tel'])) {
                if (str_replace(" ", "", $input['tel']) != "") {
                    $user->tel = ($input['tel']);
                    array_push($return_push, 'tel');
                }
            }
        }
        if (isset($input['address'])) {
            if (str_replace(" ", "", $input['address']) != "") {
                $user->address = ($input['address']);
                array_push($return_push, 'address');
            }
        }
        if (isset($input['apartment'])) {
            if (str_replace(" ", "", $input['apartment']) != "") {
                $user->apartment = ($input['apartment']);
                array_push($return_push, 'apartment');
            }
        }
        if (isset($input['entrance'])) {
            if (str_replace(" ", "", $input['entrance']) != "") {
                $user->entrance = ($input['entrance']);
                array_push($return_push, 'entrance');
            }
        }
        if (isset($input['district'])) {
            if (str_replace(" ", "", $input['district']) != "") {
                $user->district = ($input['district']);
                array_push($return_push, 'district');
            }
        }
        $user->save();

        return $this->sendResponse($return_push, 'ИНформация о пользователи изменена');
    }


    public function token_push(Request $request)
    {
        $request = $request->all();

        \App\Register::where("user_id", $this->users->id)->delete();

        $token = new \App\Register;
        $token->key = $request["token"];
        $token->user_id = $this->users->id;
        $token->save();
        return $this->sendResponse([], 'Токен сохранен');
    }

}
