<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Str;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $fieldName = "tel";
        $identity = validate_phone_number($request->tel);

//        if($identity==false){
//            return $this->sendError('Ошибка валидации.', $validator->errors());
//        }

        request()->merge([$fieldName => $identity]);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Ошибка валидации.', $validator->errors());
        }


        $input = $request->all();


        if (isset($input["email"])) {
            $email = \App\User::where("email", $input["email"])->first();
            if (!is_null($email)) {
                return $this->sendError('Error', ['error' => 'email уже занят']);
            }
        }

        if (isset($input["tel"])) {
            $phone = \App\User::where("tel", $input["tel"])->first();
            if (!is_null($phone)) {
                return $this->sendError('Error', ['error' => 'Телефон уже занят']);
            }
        }

        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('burgerApp')->accessToken;
        $success['name'] = $user->name;
        $success['email'] = $user->email;
        $success['tel'] = $user->tel;
        $success['address'] = $user->address;
        $success['apartment'] = $user->apartment;
        $success['entrance'] = $user->entrance;
        $success['district'] = $user->district;


        return $this->sendResponse($success, 'Пользователь создан успешно.');
    }


    public function user_check(Request $request)
    {
        $input = $request->all();

        $nameInput = "login";
        $fieldName = "";
        $identity = $request->login;
        if (filter_var($identity, FILTER_VALIDATE_EMAIL)) {
            $fieldName = "email";
        } else {
            $fieldName = validate_phone_number($identity) == false ? 'username' : 'tel';
        }
        if ($fieldName == "tel") {
            $identity = validate_phone_number($identity);
        }

        $users = \App\User::where($fieldName, $identity)->first();

        if (!is_null($users)) {
            return $this->sendResponse([$fieldName => $identity], 'Пользователь найден.');
        } else {
            return $this->sendError('Пользователь не найден', []);
        }

    }


    public function get_token()
    {

        $user = User::create(["name" => "", "username" => Str::random(10), "password" => ""]);
        $success['token'] = $user->createToken('burgerApp')->accessToken;

        return $this->sendResponse($success, 'ваш токен создан.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $nameInput = "login";
        $fieldName = "";
        $identity = $request->login;
        if (filter_var($identity, FILTER_VALIDATE_EMAIL)) {
            $fieldName = "email";
        } else {
            $fieldName = validate_phone_number($identity) == false ? 'username' : 'tel';
        }
        if ($fieldName == "tel") {
            $identity = validate_phone_number($identity);
        }


        if (Auth::attempt([$fieldName => $identity, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('burgerApp')->accessToken;
            $success['name'] = $user->name;
            $success['email'] = $user->email;
            $success['tel'] = $user->tel;
            $success['address'] = $user->address;
            $success['apartment'] = $user->apartment;
            $success['entrance'] = $user->entrance;
            $success['district'] = $user->district;

            \App\Basket::where("user_id", $user->id)->delete();


            return $this->sendResponse($success, 'Вы авторизированны успешно.');
        } else {
            return $this->sendError('Error', ['error' => 'Введен неверный логин или пароль']);
        }
    }
}
