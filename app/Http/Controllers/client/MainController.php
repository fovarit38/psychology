<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use App\Http\Resources\ProductGroups as ProductGroupsResource;
use App\Mail\resetShipped;
use Mail;

class MainController extends Controller
{

    public function __construct()
    {
//        $url=$_SERVER['REQUEST_SCHEME']."://".$_SERVER['REQUEST_URI']."/ru/almaty/user/";

    }

    public function index()
    {


        $grups = \App\ProductGroups::get();
        return view('views.main', compact('grups'));
    }

    public function catalog($path)
    {
        $catalog = \App\Catalog::where("path", $path)->first();

        $title = $catalog->title;
        return view('views.catalog', compact('title', 'catalog'));
    }

    public function basket()
    {
        $basket = 'Корзина';
        return view('views.basket', compact('basket'));
    }


}
