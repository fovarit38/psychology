<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Product as ProductResource;

class ProductGroupsItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $product = $this->product;

        if(!is_null($product)){
            $product->fullscreen=$this->fullscreen;
        }

        $resourse=ProductResource::collection([$product]);
        return $resourse[0];
    }
}
