<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Product as ProductResource;
class Basket extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        $product = $this->product;
        $product->setAttribute("amount",$this->amount);

        if(isset($this->avorites)){
            $product->setAttribute("favorite_id",$this->id);
        }else{
            $product->setAttribute("basket_id",$this->id);
        }

        $resourse = ProductResource::collection([$product]);
        $resourse = $resourse[0];


        return $resourse;
    }
}
