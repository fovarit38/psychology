<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function bay_basket()
    {

    }

    public function toArray($request)
    {

        $data = [
            'id' => $this->id,
            'created_at' => $this->created_at->format('d/m/Y'),
            'updated_at' => $this->updated_at->format('d/m/Y'),
        ];
        foreach ($this->attributesToArray() as $keyxs => $poasd) {
            if (!isset($data[$keyxs])) {
                $data[$keyxs] = $poasd;
            }
        }
        $data["tags"]=array_keys($this->product_tag->groupby("name")->toarray());
        $data["bay"] = rand(0,1);
        if (isset($this->fullscreen)) {
            $data["fullscreen"] = $this->fullscreen;
        }
        if (isset($this->amount)) {
            $data["amount"] = $this->amount;
        }
        if (isset($this->basket_id)) {
            $data["basket_id"] = $this->basket_id;
        }

        return $data;
    }
}
