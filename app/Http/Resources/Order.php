<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\Order_item as OrdersItemResource;


class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $status = [
            "new" => "Новый",
            "done" => "Доставлен",
            "accepted" => "Заказ обработан",
            "return" => "Возврат",
            "cancelled" => "Отменить"
        ];
        return [
            'title' => $this->title,
            'address' => $this->address,
            'comment' => $this->comment,
            'email' => $this->email,
            'phone' => $this->phone,
            'price' => $this->price,
            'order_number' => $this->order_number,
            'status' => $this->status,
            'created_at' => $this->created_at->format('d/m/Y'),
            'products' => OrdersItemResource::collection($this->items)
        ];
    }
}
