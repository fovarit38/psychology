<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromArray;
use DB;
use Maatwebsite\Excel\Events\AfterSheet;

class OrdersOneExport implements FromArray
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function array(): array
    {
        $orders = \App\Order::orderBy(DB::raw('FIELD(status, "new")'), 'desc')->where("id",$this->id)->get();
        $reutn_ar = [];
        foreach ($orders as $ars) {
            array_push($reutn_ar, [
                $ars->order_number,
                $ars->status,
                $ars->name,
                "Цена за все: " . $ars->price,
                $ars->email,
                $ars->phone,
                $ars->payment,
                $ars->delivery,
                $ars->delivery_price,
                $ars->comment,
                $ars->address,
                $ars->delivery_time,
            ]);
            foreach (\App\Order_item::where("orders_id", $ars->id)->get() as $isbox) {
                array_push($reutn_ar, ["---", "---", $isbox->title, "__Цена: " . $isbox->price, "Количество: " . $isbox->amount]);
            }
            array_push($reutn_ar, ['']);
        }
        return ($reutn_ar);
    }


}
