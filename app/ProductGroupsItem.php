<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGroupsItem extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product','Product_id','id');
    }
}
