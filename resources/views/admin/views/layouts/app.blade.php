<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }} :: Dashboard</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="/public/media/admin/css/style.css?v=3">
</head>
<body>


<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

<div class="container-fluid">
    <section class="mainsection row">
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">{{ config('app.name', 'Laravel') }}</a>
            <ul class="navbar-nav px-4">
                <li class="nav-item text-nowrap">
                    <a class="userExit" href="javascript:void(0);" style="color:#fff;" title="Выйти"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"> Выйти</a>
                </li>
            </ul>
        </nav>

        <nav class="col-md-3 col-lg-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                @php
                    $model_urlink="";
                    if(isset($model_name)){
                                        $model_urlink=  $model_name;
                    }
                    $id_saving=-1;

                    if(isset($id)){
                        $id_saving=$id;
                    }

                    $dev_status=false;

                    if (Cache::has('dev')){
                        $dev_status=Cache::get('dev');
                    }

                @endphp

                <ul class="nav flex-column">


                    @foreach(\App\Model_meta::where("type","table_catalog_availability")->where("name_key","1")->orderby("sort","desc")->get() as $navlink)

                        <li class="nav-item {{$model_urlink==$navlink->attachment?"active":""}}">
                            <a href="javascript:void(0);"
                               class="nav-link {{$model_urlink==$navlink->attachment?"active":""}} cli" data-un=""
                               data-closest=".nav-item">
                                {!! ($navlink->model_name()->icon) !!}
                                <span> {{($navlink->model_name()->name)}}</span>
                            </a>

                            <ul class="nav nav-dropmenu flex-column">
                                <li class="nav-item ">
                                    <a href="{{url_custom("/admin/model/".$navlink->attachment)}}"
                                       class="nav-link {{$model_urlink==$navlink->attachment?($id_saving==-1?"active":""):""}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-layers">
                                            <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                                            <polyline points="2 17 12 22 22 17"></polyline>
                                            <polyline points="2 12 12 17 22 12"></polyline>
                                        </svg>
                                        <span> Каталог</span>
                                    </a>
                                </li>
                                @if($navlink->attachment!="Day")
                                <li class="nav-item">
                                    <a href="{{url_custom('/admin/model/'.$navlink->attachment.'/0')}}"
                                       class="nav-link {{$model_urlink==$navlink->attachment?($id_saving==0?"active":""):""}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-plus-circle">
                                            <circle cx="12" cy="12" r="10"></circle>
                                            <line x1="12" y1="8" x2="12" y2="16"></line>
                                            <line x1="8" y1="12" x2="16" y2="12"></line>
                                        </svg>
                                        <span> Добавление записи</span>
                                    </a>
                                </li>
                                @endif
                            </ul>

                        </li>

                    @endforeach
                        <li class="nav-item ">
                            <a href="javascript:void(0);" class="nav-link  cli" data-un="" data-closest=".nav-item">
                                <span class="oi" data-glyph="rain"></span>
                                <span> Текста</span>
                            </a>

                            <ul class="nav nav-dropmenu flex-column">
                                <li class="nav-item ">
                                    @php
                                        $thead_nav = \App\StaticText::get()->groupby("page");
                                    @endphp
                                    @foreach($thead_nav as $keyxs=>$the)
                                        <a href="{{url_custom('/admin/StaticText?page='.$keyxs)}}" class="nav-link ">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24"
                                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-layers">
                                                <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                                                <polyline points="2 17 12 22 22 17"></polyline>
                                                <polyline points="2 12 12 17 22 12"></polyline>
                                            </svg>
                                            <span> {{$keyxs}}</span>
                                        </a>
                                    @endforeach
                                </li>
                            </ul>

                        </li>
                </ul>



                <a style=" position: absolute; bottom: 0; right: 0; padding: 1rem; "
                   href="{{url_custom("/admin/dev/".($dev_status?"false":"true"))}}">{{($dev_status?"debug: false":"debug: true")}}</a>
            </div>
        </nav>
        <main class="main col-md-9 ml-sm-auto col-lg-10 px-4" role="main">

            <div class="main_box">
                @yield('content')
            </div>

        </main>
    </section>
</div>
<script src="/public/media/admin/js/app.js?v=3"></script>

<script>

    $(function () {
        <?php
        if(\Session::has('alert')){
        ?>
        $.fancybox.open('<?=Session::get('alert')?>');
        <?
        }
        ?>
    });

</script>

</body>
</html>
