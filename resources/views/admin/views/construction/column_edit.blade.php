@extends('views.layouts.app')

@section('content')

    <form action="{{url_custom("/admin/update")}}" method="post">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
            <h1 class="h2">Изменение столбца : {{$id==0?"Добавление записи":"Обновление записи"}}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group ">
                    <input type="submit" class="btn btn-sm  btn-success waves-effect  px-4"  value="Применить">
                </div>
            </div>
        </div>

        <div class="bodyMain">
            <div class="bodyMain">
                <div>
                    @csrf
                    <input type="hidden" name="model_name" value="Column_name">
                    <input type="hidden" name="id" value="{{(!is_null($column)?$column->id:0)}}">

                    {{maskInput($errors,["name"=>"name_key_save","type"=>"text","placeholder"=>"Ключ"],$column)}}
                    {{maskInput($errors,["name"=>"name_save","type"=>"text","placeholder"=>"Название"],$column)}}

                    <input type="hidden" name="path" value="{{url_custom("/admin/column_name")}}">

                </div>
            </div>
        </div>
    </form>


@endsection
