<div class="row-fluid" style="margin-bottom: 1.5rem;">
    <select class="selectpicker" name="product_id_save" data-show-subtext="true" data-live-search="true">
        <option value="">Выберите товар</option>
        @foreach(\App\Product::get() as $product)
            <option value="{{$product->id}}" {{ !is_null($model)?($model->product_id==$product->id?'selected':'' )
                        : ''}} data-subtext="/{{$product->path}}">{{$product->title}}
            </option>
        @endforeach
    </select>
</div>


@php
    $product_id  = isset($_REQUEST["orders_id"])? $_REQUEST["orders_id"]:'';
    $product_id  = is_null($model)? $product_id: $model->Product_id;
@endphp

<input type="hidden" name="orders_id_save" value="{{$product_id}}">
