<div class="row-fluid" style="margin-bottom: 1.5rem;">
    <select class="selectpicker" name="Product_id_save" data-show-subtext="true" data-live-search="true">
        <option value="">Выберите товар</option>
        @foreach(\App\Product::get() as $product)
            <option value="{{$product->id}}" {{ !is_null($model)?($model->Product_id==$product->id?'selected':'' )
                        : ''}} data-subtext="/{{$product->path}}">{{$product->title}}
            </option>
        @endforeach
    </select>
</div>

<div class="row-fluid" style="margin-bottom: 1.5rem;">
    <p style="width: 100%;">
        fullscreen
    </p>
    <select class="selectpicker" name="fullscreen_save" data-show-subtext="true" data-live-search="true">
        <option value="0" {{!is_null($model)? ($model->fullscreen==0?'selected':''):''}} >Нет</option>
        <option value="1" {{!is_null($model)? ($model->fullscreen==1?'selected':''):''}}>Да</option>
    </select>
</div>

@php

    $product_id  = isset($_REQUEST["ProductGroups_id"])? $_REQUEST["ProductGroups_id"]:'';


    $product_id  = is_null($model)? $product_id: $model->product_groups_id;


@endphp

<input type="hidden" name="product_groups_id_save" value="{{$product_id}}">
<input type="hidden" name="path" value="{{url_custom("/admin/model/ProductGroups/".$product_id)}}">
