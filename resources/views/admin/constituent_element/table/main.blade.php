@php
    $model = app("\App\\" . $model_name);
    $columns = Schema::getColumnListing($model->getTable());
@endphp
<table
    class="table sortTables @if(in_array("sort",$columns)) sortable  @endif   table-bordered table-striped js-table dataTable order-table no-footer"
    id="DataTables_Table_0" role="grid"  data-action="/ru/almaty/admin/model/{{$model_name}}"   aria-describedby="DataTables_Table_0_info">
    <thead>
    <tr role="row">

        @foreach($thead as $head)
            <th class="sorting {{$head=="id"?"fitwidth fitwidth-mini":""}}" tabindex="0"
                aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="">
                {{$head}}
            </th>
        @endforeach
        <td class="fitwidth">
            Действие
        </td>
    </tr>
    </thead>
    <tbody class="ui-sortable">


    @foreach($tbody as $body)
        <tr role="row" class="even clickNexts ui-sortable-handle">
            @foreach($thead as $keyxs=>$head)
                <td class=" sorting_1 {{$keyxs=="id"?'fitwidth fitwidth-mini index':""}}" {{$keyxs=="id"?" data-id=".$body->id."":""}}>
                    @php
                        $return_data=($body->{$keyxs});
                    @endphp

                    @if($keyxs=="Product_id")
                        @php
                            $product=\App\Product::find($body->{$keyxs});
                                if(!is_null($product)){
                                $return_data=$product->title;
                                }
                        @endphp
                    @endif

                    @if(isset($exegesis))
                        @if(isset($exegesis[$keyxs]))
                            @if(isset($exegesis[$keyxs][$body->{$keyxs}]))
                                @php
                                    $return_data=  $exegesis[$keyxs][$body->{$keyxs}];
                                @endphp
                            @endif
                        @endif
                    @endif



                    {!! $return_data !!}

                </td>
            @endforeach
            <td class="fitwidth">

                @if($model_name=="Catalog" || $model_name=="Product" || $model_name=="Order_item")
                    <a href="{{url_custom($table_link[0].$body->{$table_link[1]}.'/remove')}}"
                       style="color:red;margin-right: 1rem;">Удалить</a>
                @endif
                @if($model_name=="Catalog")
                    @if($body->visable=="1")
                        <a href="{{url_custom($table_link[0].$body->{$table_link[1]}.'/hidden/0')}}"
                           style="color:red;margin-right: 1rem;">Скрыть</a>
                    @else
                        <a href="{{url_custom($table_link[0].$body->{$table_link[1]}.'/hidden/1')}}"
                           style="color:green;margin-right: 1rem;">Раскрыть</a>
                    @endif
                @elseif($model_name=="Product")
                    @if($body->visable=="1")
                        <a href="{{url_custom($table_link[0].$body->{$table_link[1]}.'/hidden/0')}}"
                           style="color:red;margin-right: 1rem;">Скрыть</a>
                    @else
                        <a href="{{url_custom($table_link[0].$body->{$table_link[1]}.'/hidden/1')}}"
                           style="color:green;margin-right: 1rem;">Раскрыть</a>
                    @endif
                @else


                    @if($model_name!="Day" && $model_name!="Order_item"  )
                        <a href="{{url_custom($table_link[0].$body->{$table_link[1]}.'/remove')}}"
                           style="color:red;margin-right: 1rem;">Удалить</a>
                    @endif
                @endif


                <a class="nextlink" href="{{url_custom($table_link[0].$body->{$table_link[1]})}}">Изменить</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
