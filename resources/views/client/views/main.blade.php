@extends('views.layouts.app')

@section('content')
    @php
        $json=  file_get_contents('../storage/Burger.postman_collection.json');
        $json= json_decode($json);
        $json=$json->requests;
    @endphp

    @if(isset($_REQUEST["id"]))
        @php
            $found_key = array_search($_REQUEST["id"], array_column($json, 'id'));
            $js=($json[$found_key]);
        @endphp
        <div class="container json_block">
            <form class="lising text text-s16 dana_info">
                <b>{{$js->name}}</b> <br>
                Method: <b> {{$js->method}}</b> <br>
                url: {{str_replace("cicada.kz",$_SERVER['HTTP_HOST'],$js->url)}} <br>
                @if(isset($js->headerData)?count($js->headerData)>0:false)
                    <b>- Header </b>
                    @foreach($js->headerData as $ds)
                        <br>
                        <div class="inputs">
                            <div class="input-box">
                                <div class="input-text">{{($ds->key)}}</div>
                                <input type="text" name="header[{{($ds->key)}}]" class="input">
                            </div>
                        </div>
                    @endforeach
                @else
                    <b>Header не требуется</b>
                @endif
                <br>
                @if(isset($js->data)?count($js->data)>0:false)
                    <b>- Есть возможност отправить данные</b>
                    @foreach($js->data as $ds)
                        <div class="inputs">
                            <div class="input-box">
                                <div class="input-text">{{($ds->key)}}</div>
                                <input type="text" name="{{($ds->key)}}" class="input">
                            </div>
                        </div>
                    @endforeach
                @else
                    <b>Нет возможности отправить данные</b>
                @endif
            </form>

            <a href="javascript:void(0)" data-json=".deks" data-return=".return_arra" data-type="{{$js->method}}"
               data-action="{{str_replace("https://cicada.kz","",$js->url)}}"
               class="btn btn-add get-api">
                <span class="text text-s14">отправить запрос</span>
            </a>
            <div class="lising">
                <p class="text text-s16">
                    Результат
                </p>
            </div>
            <div class="return_arra tex_return"></div>
        </div>
    @else
        @foreach($json as $js)
            <div class="container json_block">
                <div class="lising">
                    <p class="text text-s16">

                        <b>{{$js->name}}</b> <br>
                        Method: <b> {{$js->method}}</b> <br>
                        url: {{str_replace("cicada.kz",$_SERVER['HTTP_HOST'],$js->url)}} <br>
                        @if(isset($js->headerData)?count($js->headerData)>0:false)
                            <b>- Header </b>
                            @foreach($js->headerData as $ds)
                                <br>
                                -- {{($ds->key)}}
                            @endforeach
                        @else
                            <b>Header не требуется</b>
                        @endif
                        <br>
                        @if(isset($js->data)?count($js->data)>0:false)
                            <b>- Есть возможност отправить данные</b>
                            @foreach($js->data as $ds)
                                <br>
                                -- {{($ds->key)}}
                            @endforeach
                        @else
                            <b>Нет возможности отправить данные</b>
                        @endif
                        <br>
                        <br>
                        <a href="{{url_custom('/?id='.$js->id)}}" class="btn" style="color:green;">использовать</a>
                    </p>
                </div>

                {{--            <a href="javascript:void(0)" data-json=".deks" data-return=".return_arra" data-type="get"--}}
                {{--               data-action="/api/products/top"--}}
                {{--               class="btn btn-add get-api">--}}
                {{--                <span class="text text-s14">отправить запрос</span>--}}
                {{--            </a>--}}
                {{--            <div class="lising">--}}
                {{--                <p class="text text-s16">--}}
                {{--                    Результат--}}
                {{--                </p>--}}
                {{--            </div>--}}
                {{--            <div class="return_arra tex_return"></div>--}}
            </div>
        @endforeach
    @endif
    <style>
        .lising {
            background-color: #efefef;
            padding: 1rem;
        }
    </style>

@endsection
