@extends('views.layouts.app')

@section('content')

    <div class="container">


        <div class="baskets">

            @for($cs=0;$cs<2;$cs++)
                <div class="basket-item">
                    <div class="basket-item_title">
                        <div class="title title-baskets">
                            <span class="text text-s16"><b>Escoreze</b></span>
                            <span class="text text-s18"><b>550 тг</b></span>
                        </div>
                    </div>
                    <div class="basket-item_detalis">
                        <div class="basket-item_img">
                            <div class="prop">
                                <div class="prop_img prop_img-basket">
                                    <div class="prop_img_src"
                                         style="background-image: url('/public/media/client/images/product/04.png');">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="basket-item_increase">
                            <div class="increase">
                                <a href="javascript:void(0)" class="btn btn-inc"><img
                                        src="/public/media/client/images/basket/icon/plus.png" alt=""></a>
                                <p><span class="text text-s20">1</span></p>
                                <a href="javascript:void(0)" class="btn btn-inc btn-inc-minus"></a>

                            </div>
                        </div>
                        <div class="basket-item_del">
                            <span class="text text-s12" style="color: #5E5E5E;">160 гр. </span>
                            <a href="javascript:void(0)" class="btn btn-delete">
                                <img src="/public/media/client/images/basket/icon/delete.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            @endfor


            <div class="result-order">
                <div class="result-order_select">
                    <p class="text text-s14">
                        Выберите район доставки
                    </p>
                    <div class="result-order_sel">
                        <select class="select-custom text text-s14">
                            <option value="">Жетысуйский</option>
                        </select>
                    </div>
                </div>
                <div class="result-order_result">
                    <div class="result-order_detalis">
                        <p class="text text-s16">
                            <b>Итого:</b>
                        </p>
                        <p class="text text-s14">
                            <span>Доставка:</span>
                            <b>800 тг.</b>
                        </p>
                        <p class="text text-s14">
                            <span>Сумма 2 товаров:</span>
                            <b>2100 тг.</b>
                        </p>
                        <p class="text text-s14">
                            <span>Итого к оплате:</span>
                            <b>2900 тг.</b>
                        </p>

                    </div>
                    <div class="btn btn-add">
                        <span class="text text-s14">Заказать на 2990тг.</span>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection
