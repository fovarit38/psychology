@php

    @endphp

<div class="product {{isset($fullscreen)?($fullscreen=="1"?'product-full':''):''}} ">
    <div class="product_img">
        <div class="prop">
            <div class="prop_img">
                <div class="prop_img_src"
                     style="background-image: url('{{$product->images}}');">

                </div>
            </div>
        </div>
    </div>
    <div class="product_detalis">
        <div class="title title-product">
            <p class="text text-s16">
                <b>{{$product->title}}</b>
            </p>
        </div>
        <div class="content content-product">
            <p class="text text-s12">
                {{$product->content}}
            </p>
        </div>
        <div class="feature">
            <span class="text text-s12">{{$product->ed_massa}}</span>
            <span class="text text-s14"> <b>  {{$product->price}} тг</b></span>
        </div>

        <div class="request-nav">
            <a href="javascript:void(0)" data-id="{{$product->id}}" class="btn btn-add order-add">
                <span class="text text-s14">Заказать</span>
            </a>
        </div>
    </div>
</div>
