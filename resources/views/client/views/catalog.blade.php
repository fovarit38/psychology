@extends('views.layouts.app')

@section('content')

    <div class="container">


        <div class="products products-catalog">


            @foreach($catalog->products as $product)
                @include('views.components.product')
            @endforeach

        </div>

    </div>




@endsection
