@extends('views.layouts.app')

@section('content')

    <div class="container catalog">
        <div class="breadcrumbs text text-s12">

            <a href="/"> Главная</a> <i>⟩</i> <a href="/login">Логин</a> <i>⟩</i> <b>Регистрация</b>

        </div>
        <h1 class="titleh2 text text-s26">
            РЕГИСТРАЦИЯ
        </h1>
        <div class="bkSingPodloska bkSingPodloska-mark ">
            <div class="container_product">
                <div class="loginCOntrol">


                    <div class="bulpForm ">

                        <div class="bulpForm_body ">
                            <div class="bulpForm_body_head">
                                <h2 class="msg text text-s18">ВВЕДИТЕ ВАШИ ДАННЫЕ</h2>
                            </div>
                            <p class="text text-s12 bulpForm_body_after" style="color: #ff0000;">
                                *Поля обязательные для заполнения
                            </p>


                            <form id="sign_in" role="form" class="bulpForm_body_form" method="POST" autocomplete="off"
                                  action="/api/register">
                                {{ csrf_field() }}


                                <div class="formBing hiding active">

                                    {{maskInput($errors,["name"=>"name","type"=>"text","placeholder"=>"Ваше имя","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"tel","type"=>"tel","placeholder"=>"Телефон","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"email","type"=>"email","placeholder"=>"E-mail","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"password","type"=>"password","placeholder"=>"Пароль","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"password_confirmation","type"=>"password","placeholder"=>"Введите пароль повторно","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"district","type"=>"text","placeholder"=>"Район доставки","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"address","type"=>"text","placeholder"=>"Адресс доставки","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"apartment","type"=>"text","placeholder"=>"Номер квартиры","auto"=>false])}}
                                    {{maskInput($errors,["name"=>"entrance","type"=>"text","placeholder"=>"Подъезд","auto"=>false])}}
                                </div>


                                <div class="form-group form-link">
                                    <label class="text text-s13 "></label>
                                    <button class="btn  btn-success flat-buttons   waves-effect" type="submit">
                                        Регистрация
                                    </button>
                                </div>
                            </form>


                        </div>
                    </div>


                </div>


            </div>
        </div>
    </div>

@endsection


