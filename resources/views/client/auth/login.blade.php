@extends('views.layouts.app')

@section('content')

<div class="container catalog">
    <div class="breadcrumbs text text-s12">

        <a href="/"> Главная</a> <i>⟩</i> <b>Собаки</b>

    </div>
    <h1 class="titleh2 text text-s26">
        ВХОД ИЛИ РЕГИСТРАЦИЯ
    </h1>
    <div class="bkSingPodloska bkSingPodloska-mark ">
        <div class="container_product">
            <div class="loginCOntrol">


                <div class="bulpForm ">

                    <div class="bulpForm_body ">
                        <div class="bulpForm_body_head">
                            <h2 class="msg text text-s18">ВХОД В ЛИЧНЫЙ КАБИНЕТ</h2>
                        </div>
                        <p class="text text-s12 bulpForm_body_after">
                            Рады видеть вас снова!
                        </p>
                        <form id="sign_in" class="bulpForm_body_form" role="form" method="POST"
                              action="/api/login">
                            {{ csrf_field() }}


                            {{maskInput($errors,["name"=>"login","type"=>"tel","altname"=>['tel','username'],"placeholder"=>"ТЕЛЕФОН"])}}
                            {{maskInput($errors,["name"=>"password","type"=>"password","placeholder"=>"Пароль"])}}


                            {{ $errors->has('password') ? ' error' : '' }}

                            <div class="autocheck">

                                <div class="form-check">

                                </div>

                                <div class="form-group form-link">
                                    <label  class="text text-s13 "></label>

                                    <div class="custom-control custom-checkbox">
                                        <label class="containerCheck">Запомнить
                                            меня
                                            <input type="checkbox"  name="remember" checked="checked">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group form-link">
                                    <label  class="text text-s13 "></label>
                                    <button type="submit" class="btn  btn-success text text-s13 ">Войти</button>


                                </div>

                                <div class="form-group form-link">
                                    <label  class="text text-s13 "></label>
                                    <a href="/password/reset" class="text text-s13 linkLogin">
                                        Забыли пароль?
                                    </a>
                                </div>


                            </div>
                            <!--                <div class="navlink">-->
                            <!--                    <a href="{{ url('/register') }}">Регистрация</a>-->
                            <!--                    <a href="{{ url('/password/reset') }}">Забыли пароль?</a>-->
                            <!--                </div>-->
                        </form>
                    </div>
                </div>



                <div class="bulpForm ">

                    <div class="bulpForm_body ">
                        <div class="bulpForm_body_head">
                            <h2 class="msg text text-s18">РЕГИСТРАЦИЯ</h2>
                        </div>

                        <p class="text text-s12 bulpForm_body_after">
                            Первый раз у нас? Тогда самое время присоедениться к нам! Вас ждет огромный выбор товров, на любой вкус вшего питомца.
                            А так же интересные предложения и скидки.
<br>
<br>
                            Регитсрация займет у вас всего пару минут, ваш питомец даже не успеет соскучиться!
                        </p>

                        <a href="register"  class="btn  btn-success btn-success-sing text text-s13 ">СОЗДАТЬ АККАУНТ</a>


                    </div>
                </div>


            </div>



        </div>
    </div>
</div>

@endsection
